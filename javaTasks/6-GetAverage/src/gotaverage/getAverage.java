package gotaverage;

public class getAverage {
	
	public static int gotAverage(int[] numbers){
		
		assert(numbers != null);
		assert(numbers.length != 0);
		
		int sumOfNumbersInArray = 0;
		for (int i = 0; i < numbers.length; i++) {
			sumOfNumbersInArray += numbers[i];
		}
		return (int)sumOfNumbersInArray/numbers.length;
	}

	public static void main(String[] args) {
		int[] numbers = {2,4,6,3,5,1};
		System.out.println(gotAverage(numbers));
	}

}
