package maxspan;

import static org.junit.Assert.*;

import org.junit.Test;

import maxspan.MaxSpan;

public class MaxSpanTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] nullArray = null;
		MaxSpan.maxSpan(nullArray);
	}

	@Test(expected = AssertionError.class)
	public void testEmptyArrays() {
		int[] empty = {};
		MaxSpan.maxSpan(empty);
	}
	
	@Test
	public void testNoSameElementsInArray(){
		int[] diverse = {1,5,7,9,3,4,2,6};
		int result = MaxSpan.maxSpan(diverse);
		assertEquals(1, result);
	}
	
	@Test
	public void testOnlySameElementsInArray(){
		int[] sameOnes = {1,1,1,1,1,1,1,1};
		int result = MaxSpan.maxSpan(sameOnes);
		assertEquals(8, result);
	}
	
	@Test
	public void testMaxSpan(){
		int[] numbers = {8, 12, 7, 1, 7, 2, 12};
		int result = MaxSpan.maxSpan(numbers);
		assertEquals(6, result);
	}

}
