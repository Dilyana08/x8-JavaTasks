package equalsumsides;

public class EqualSumSides {
	// Kaloyan`s algorithm

	public static boolean ┼qualSumSides(int[] numbers) {
		assert(numbers != null);
		assert(numbers.length != 0);
		
		if (numbers.length == 1) {
			return true;
		}

		int leftSum = 0;
		int rightSum = SumOfNumbers(numbers);

		for (int i = 0; i < numbers.length; i++) {
			rightSum -= numbers[i];
								
			if (leftSum == rightSum){
				return true;
			}
			
			leftSum += numbers[i];
		}
		return false;
	}

	private static int SumOfNumbers(int[] numbers) {
		int sumOfNumbersInArray = 0;
		
		for (int i = 0; i < numbers.length; i++) {
			sumOfNumbersInArray += numbers[i];
		}
		
		return sumOfNumbersInArray;
	}

	public static void main(String[] args) {
		int[] falseArray = {8,8};
		int[] trueArray = {3, 0, -1, 2, 1};
		System.out.println(┼qualSumSides(falseArray));
		System.out.println(┼qualSumSides(trueArray));
	}

}
