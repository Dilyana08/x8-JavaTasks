package kthfactorial;

import static org.junit.Assert.*;

import org.junit.Test;

import kthfactorial.KthFactorial;

public class KthFactorialTest {

	@Test(expected = AssertionError.class)
	public void testNegativeNumber() {
		KthFactorial.kthFac(3, -5);
	}
	
	@Test
	public void testThirdFactorialOfZero(){
		long result = KthFactorial.kthFac(3, 0);
		assertEquals(1, result);
	}
	
	@Test
	public void testFifthFactorialOfOne(){
		long result = KthFactorial.kthFac(5, 1);
		assertEquals(1, result);
	}
	
	@Test
	public void testSecondFactorialOfThree(){
		long result = KthFactorial.kthFac(2, 3);
		assertEquals(720, result);
	}

}
