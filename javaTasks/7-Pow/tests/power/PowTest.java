package power;

import static org.junit.Assert.*;

import org.junit.Test;

import power.Pow;

public class PowTest {

	@Test
	public void testZeroPow(){
		long result = Pow.pow(2, 0);
		assertEquals(1, result);
	}
	
	@Test
	public void testOnePow(){
		long result = Pow.pow(2, 1);
		assertEquals(2, result);
	}
	
	@Test
	public void testEvenPow(){
		long result = Pow.pow(2, 2);
		assertEquals(4, result);
	}
	
	@Test
	public void testOddPow(){
		long result = Pow.pow(2, 3);
		assertEquals(8, result);
	}
	
	@Test
	public void testZeroBase(){
		long result = Pow.pow(0, 2);
		assertEquals(0, result);
	}
	
	@Test
	public void testOneBase(){
		long result = Pow.pow(1, 10);
		assertEquals(1, result);
	}

}
