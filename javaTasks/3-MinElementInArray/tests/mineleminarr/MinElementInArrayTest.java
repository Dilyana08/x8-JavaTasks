package mineleminarr;

import static org.junit.Assert.*;

import org.junit.Test;

import mineleminarr.MinElementInArray;

public class MinElementInArrayTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] nullArray = null;
		MinElementInArray.minElement(nullArray);
	}

	@Test(expected = AssertionError.class)
	public void testEmptyArrays() {
		int[] empty = {};
		MinElementInArray.minElement(empty);
	}
	
	@Test
	public void testMinElementTwoElementsArray(){
		int[] twoElements = {-5, 20};
		int result = MinElementInArray.minElement(twoElements);
		assertEquals(-5, result);
	}
	
	@Test
	public void testMinElementSameElementsArray(){
		int[] sameElements = {2, 2, 2, 2, 2, 2};
		int result = MinElementInArray.minElement(sameElements);
		assertEquals(2, result);
	}
	
	@Test
	public void testMinElementInArray(){
		int[] numbers = {16,22,-3,0,86,-45,333,-621,8};
		int result = MinElementInArray.minElement(numbers);
		assertEquals(-621, result);
	}

}
