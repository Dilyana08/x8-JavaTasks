package power;

public class Pow {
	
	public static long pow(int base, int power) {
		if (power == 0) {
			return 1;
		}
		if (power == 1) {
			return base;
		}

		long p = pow(base, power / 2);
		if (isOdd(power)) {
			return base * p * p;
		} else {
			return p * p;
		}
	}

	private static boolean isOdd(int number) {
		return (number % 2 != 0);
	}

	public static void main(String[] args) {
		System.out.println(pow(2, 3));
	}

}
