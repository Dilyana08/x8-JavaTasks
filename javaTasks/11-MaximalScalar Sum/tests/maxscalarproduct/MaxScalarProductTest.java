package maxscalarproduct;

import static org.junit.Assert.*;

import org.junit.Test;

import maxscalarproduct.MaxScalarProduct;

public class MaxScalarProductTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] first = { -5, 20, 0, 17, -5, 44 };
		int[] second = null;
		MaxScalarProduct.maximalScalarProduct(first, second);
	}

	@Test
	public void testEmptyArrays() {
		int[] first = {};
		int[] second = {};
		long result = MaxScalarProduct.maximalScalarProduct(first, second);
		assertEquals(0, result);
	}

	@Test(expected = AssertionError.class)
	public void testDifferentLengthArrays() {
		int[] first = { -5, 20, 0, 17, -5, 44 };
		int[] second = { 54, 16, 9, 4 };
		MaxScalarProduct.maximalScalarProduct(first, second);
	}
	
	@Test
	public void testMaxScalarProduct() {
		int[] first = { -5, 20, 0, 17, -5, 44 };
		int[] second = { 54, 16, 9, 4, 12, 3 };
		long result = MaxScalarProduct.maximalScalarProduct(first, second);
		assertEquals(2865, result);
	}
	

}
