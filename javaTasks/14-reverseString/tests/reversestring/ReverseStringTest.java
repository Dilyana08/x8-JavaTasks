package reversestring;

import static org.junit.Assert.*;

import org.junit.Test;

import reversestring.ReverseString;

public class ReverseStringTest {

	@Test(expected = AssertionError.class)
	public void testNullString() {
		String nullString = null;
		ReverseString.reverse(nullString);
	}
	
	@Test
	public void testEmptyString() {
		String empty = "";
		ReverseString.reverse(empty);
		assertEquals("", empty);
	}
	
	@Test
	public void testShortString() {
		String shortString = "Out, out, brief candle!";
		String expected = "!eldnac feirb ,tuo ,tuO";
		String result = ReverseString.reverse(shortString);
		assertEquals(expected, result);
	}
	
	@Test
	public void testLongString() {
		String longString = "To die, to sleep To sleep perchance to dream...";
		String expected = "...maerd ot ecnahcrep peels oT peels ot ,eid oT";
		String result = ReverseString.reverse(longString);
		assertEquals(expected, result);
	}
}
