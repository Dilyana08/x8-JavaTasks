package doublefactorial;

import static org.junit.Assert.*;

import org.junit.Test;

import doublefactorial.DoubleFactorial;

public class DoubleFactorialTest {

	@Test(expected = AssertionError.class)
	public void testNegativeNumber() {
		DoubleFactorial.doubleFac(-12);
	}
	
	@Test
	public void testZeroFactorial(){
		long result = DoubleFactorial.doubleFac(0);
		assertEquals(1, result);
	}
	
	@Test
	public void testOneFactorial(){
		long result = DoubleFactorial.doubleFac(1);
		assertEquals(1, result);
	}
	
	@Test
	public void testSmallFactorial(){
		long result = DoubleFactorial.doubleFac(3);
		assertEquals(720, result);
	}

}
