package maxspan;

public class MaxSpan {

	public static int maxSpan(int[] numbers) {
		assert(numbers != null);
		assert(numbers.length != 0);
		
		int maxSpan = 1;

		for (int i = 0; i < numbers.length; i++) {
			for (int j = numbers.length - 1; j > i; j--) {
				if (numbers[i] == numbers[j]) {
					if (j - i > maxSpan) {
						maxSpan = j - i + 1;
					}
					break;
				}
			}
		}
		return maxSpan;
	}

	public static void main(String[] args) {
		int[] numbers = {8, 12, 7, 1, 7, 2, 12};
		System.out.println(maxSpan(numbers));

	}

}
