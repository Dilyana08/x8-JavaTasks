package kthmineleminarr;

public class KthMinElementInArray {

	public static int kthMinElement(int k, int[] numbers){
		
		assert(numbers != null);
		assert (numbers.length != 0);
		assert(k > 0 && k < numbers.length);
		
		return quickSelect(numbers, k - 1);
	}
	
	private static int quickSelect(int[] numbers, int elementIndex)
    {
        assert(numbers != null);
        assert(elementIndex >= 0 &&
               elementIndex < numbers.length);
       
        int start = 0;
        int end = numbers.length - 1;
        int pivotIndex = partition(numbers, start, end);
        
        do
        {
            if(pivotIndex > elementIndex)
            {
                end = pivotIndex - 1;
            }
            else
            {
                start = pivotIndex + 1;
            }
    
            pivotIndex = partition(numbers, start, end);
            
        }while(pivotIndex != elementIndex);
        
        return numbers[elementIndex];
    }
	
	private static int partition(int[] numbers, int start, int end){
		assert(numbers != null);
		assert(start >= 0);
		assert(end >= 0 && end < numbers.length);
		
		int pivot = numbers[end];
		int pivotIndex = start;
		
		for (int i = start; i < end; i++)
	    {
	       if(numbers[i] < pivot){
	        	swap(numbers, pivotIndex, i);
	            pivotIndex++;
	           }
	    }
	    swap(numbers, pivotIndex, end);
	    
	    return pivotIndex;
	}
	
	private static void swap(int[] numbers, int firstIndex, int secondIndex){
		int temp = numbers[firstIndex];
        numbers[firstIndex] = numbers[secondIndex];
        numbers[secondIndex] = temp;
	}
	
	
	public static void main(String[] args) {
		int[] numbers = {16,22,-3,0,86,-45,333,-621,8};
		System.out.println(kthMinElement(3, numbers));
		
		int[] empty = {};
		System.out.println(kthMinElement(2, empty));

	}

}
