package doublefactorial;

public class DoubleFactorial {

	private static long factorial(long number) {
		assert (number >= 0);

		if (number == 0) {
			return 1;
		} else {
			return (number * factorial(number - 1));
		}
	}

	public static long doubleFac(int number) {
		assert (number >= 0);

		if (number == 0) {
			return 1;
		}

		long fact = factorial(number);
		long doubleFactorial = factorial(fact);

		return doubleFactorial;
	}

	public static void main(String[] args) {
		System.out.println(doubleFac(3));
	}

}
