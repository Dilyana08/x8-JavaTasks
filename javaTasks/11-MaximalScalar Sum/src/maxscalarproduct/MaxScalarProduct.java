package maxscalarproduct;

import java.util.Arrays;

public class MaxScalarProduct {
	
	public static long maximalScalarProduct(int[] first, int[] second){
		assert(first != null);
		assert(second != null);
		assert(first.length == second.length);
		
		long maxSum = 0;
		Arrays.sort(first);
		Arrays.sort(second);
		
		for (int i = 0; i < first.length; i++) {
			maxSum += first[i] * second[i];
		}
		return maxSum;
	}
	
	public static void main(String[] args) {
		int[] first = {4,8,4,6,14,1,9,41,58};
		int[] second = {9,1,6,4,5,7,2,5,1,521,54};
		System.out.println(maximalScalarProduct(first, second));
	}

}
