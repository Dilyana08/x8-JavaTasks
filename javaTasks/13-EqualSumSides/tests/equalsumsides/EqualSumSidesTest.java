package equalsumsides;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sun.javafx.scene.control.behavior.TwoLevelFocusBehavior;

import equalsumsides.EqualSumSides;

public class EqualSumSidesTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] nullArray = null;
		EqualSumSides.┼qualSumSides(nullArray);
	}

	@Test(expected = AssertionError.class)
	public void testEmptyArrays() {
		int[] empty = {};
		EqualSumSides.┼qualSumSides(empty);
	}

	@Test
	public void testOnlyEvenNumberOfSameElementsInArray() {
		int[] sameOnes = { 1, 1, 1, 1, 1, 1, 1, 1 };
		boolean result = EqualSumSides.┼qualSumSides(sameOnes);
		assertEquals(false, result);
	}

	@Test
	public void testOnlyOddNumberOfSameElementsInArray() {
		int[] sameOnes = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		boolean result = EqualSumSides.┼qualSumSides(sameOnes);
		assertEquals(true, result);
	}

	@Test
	public void testFalseEqualSumSides() {
		int[] twoElems = { 8, 1, 14, 7, 8 };
		boolean result = EqualSumSides.┼qualSumSides(twoElems);
		assertEquals(false, result);
	}

	@Test
	public void testTrueEqualSumSides() {
		int[] numbers = { 3, 0, -1, 2, 1 };
		boolean result = EqualSumSides.┼qualSumSides(numbers);
		assertEquals(true, result);
	}
}
