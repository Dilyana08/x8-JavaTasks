package oddnumbers;

import static org.junit.Assert.*;

import org.junit.Test;
import oddnumbers.isOddNumber;

public class isOddNumberTest {

	@Test
	public void testIsOddNegative(){
		boolean result = isOddNumber.isOdd(-15);
		assertEquals(true, result);
	}
	
	@Test
	public void testIsOddZero(){
		boolean result = isOddNumber.isOdd(0);
		assertEquals(false, result);
	}
	
	@Test
	public void testIsOddTrue(){
		boolean result = isOddNumber.isOdd(75);
		assertEquals(true, result);
	}
	
	@Test
	public void testIsOddFalse(){
		boolean result = isOddNumber.isOdd(22);
		assertEquals(false, result);
	}

}
