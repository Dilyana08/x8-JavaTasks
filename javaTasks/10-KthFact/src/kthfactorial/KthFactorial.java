package kthfactorial;

public class KthFactorial {

	private static long factorial(long number) {
		assert (number >= 0);

		if (number == 0) {
			return 1;
		} else {
			return (number * factorial(number - 1));
		}
	}

	public static long kthFac(int k, long number) {
		assert (number >= 0);

		if (number == 0) {
			return 1;
		}

		long factorialOfNumber = factorial(number);

		for (int i = 1; i < k; i++) {
			factorialOfNumber = factorial(factorialOfNumber);
		}
		return factorialOfNumber;
	}

	public static void main(String[] args) {
		System.out.println(kthFac(2, 3));

	}

}
