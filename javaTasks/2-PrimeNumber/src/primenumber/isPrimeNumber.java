package primenumber;

public class isPrimeNumber {

	public static boolean isPrime(int number) {
		assert (number >= 0);
		
		if (number < 2) {
			return false;
		}

		for (int i = 2; i < Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {
		System.out.println(isPrime(5));
	}

}
