package kthmineleminarr;

import static org.junit.Assert.*;

import org.junit.Test;

import kthmineleminarr.KthMinElementInArray;

public class KthMinElementInArrayTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] nullArray = null;
		KthMinElementInArray.kthMinElement(2, nullArray);
	}

	@Test(expected = AssertionError.class)
	public void testEmptyArrays() {
		int[] empty = {};
		KthMinElementInArray.kthMinElement(2, empty);
	}
	
	@Test
	public void testKthMinElementTwoElementsArray(){
		int[] twoElements = {-5, 20, 0, 17, -5, 44};
		int result = KthMinElementInArray.kthMinElement(2, twoElements);
		assertEquals(-5, result);
	}
	
	@Test
	public void testKthMinElementSameElementsArray(){
		int[] sameElements = {2, 2, 2, 2, 2, 2};
		int result = KthMinElementInArray.kthMinElement(4, sameElements);
		assertEquals(2, result);
	}
	
	@Test
	public void testKthMinElementInArray(){
		int[] numbers = {16,22,-3,0,86,-45,333,-621,8};
		int result = KthMinElementInArray.kthMinElement(3, numbers);
		assertEquals(-3, result);
	}

}
