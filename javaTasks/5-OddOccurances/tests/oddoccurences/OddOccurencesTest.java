package oddoccurences;

import static org.junit.Assert.*;

import org.junit.Test;

import oddoccurences.OddOccurences;

public class OddOccurencesTest {
	@Test
	public void testEmptyArray(){
		int[] emptyArray = {};
		int result = OddOccurences.getOddOccurrence(emptyArray);
		assertEquals(0, result);
	}
	
	@Test
	public void testNoOddOccurences(){
		int[] evenOccurences = {1,2,3,4,1,3,5,4,2,5};
		int result = OddOccurences.getOddOccurrence(evenOccurences);
		assertEquals(0, result);
	}
	
	@Test
	public void testOddOccurencesInTheBeginning(){
		int[] oddInBeginning = {1,2,3,4,1,3,5,4,2,5,1};
		int result = OddOccurences.getOddOccurrence(oddInBeginning);
		assertEquals(1, result);
	}
	
	@Test
	public void testOddOccurencesInTheMiddle(){
		int[] oddInMiddle = {1,2,3,4,3,1,3,5,4,2,5};
		int result = OddOccurences.getOddOccurrence(oddInMiddle);
		assertEquals(3, result);
	}
	
	@Test
	public void testSameNumbers(){
		int[] sameNumbersArray = {5,5,5,5,5};
		int result = OddOccurences.getOddOccurrence(sameNumbersArray);
		assertEquals(5, result);
	}
	

}
