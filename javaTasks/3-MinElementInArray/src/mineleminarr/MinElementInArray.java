package mineleminarr;

public class MinElementInArray {

	public static int minElement(int... array){
		assert(array != null);
		assert(array.length != 0);
		
		int min = array[0];
		
		for(int i = 1; i < array.length; i++){
			if(array[i] < min){
				min = array[i];
			}
		}
		return min;
	}
	
	public static void main(String[] args) {
		int[] numbers = {16,22,-3,0,86,-45,333,-621,8};
		System.out.println(minElement(numbers));
		
		int[] empty = {};
		System.out.println(minElement(empty));
	}

}
