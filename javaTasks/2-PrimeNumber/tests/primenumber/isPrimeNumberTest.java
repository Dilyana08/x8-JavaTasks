package primenumber;

import static org.junit.Assert.*;

import org.junit.Test;

import primenumber.isPrimeNumber;

public class isPrimeNumberTest {

	@Test(expected = AssertionError.class)
	public void testNegativeNumber() {
		isPrimeNumber.isPrime(-12);
	}
	
	@Test
	public void testisPrimeOne(){
		boolean result = isPrimeNumber.isPrime(1);
		assertEquals(false, result);
	}
	
	@Test
	public void testisPrimeZero(){
		boolean result = isPrimeNumber.isPrime(0);
		assertEquals(false, result);
	}
	
	@Test
	public void testisPrimeFalse(){
		boolean result = isPrimeNumber.isPrime(15);
		assertEquals(false, result);
	}
	
	@Test
	public void testisPrimeTrue(){
		boolean result = isPrimeNumber.isPrime(5);
		assertEquals(true, result);
	}

}
