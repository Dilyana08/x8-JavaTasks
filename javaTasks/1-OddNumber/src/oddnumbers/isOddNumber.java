package oddnumbers;

public class isOddNumber {

	public static boolean isOdd(int number){
		return (number % 2 != 0);
	}
	
	public static void main(String[] args) {
		System.out.println(isOdd(15));
	}

}
