package reversestring;

public class ReverseString {

	public static String reverse(String argument) {
		assert(argument != null);
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = argument.length() - 1; i >= 0; i--) {
			stringBuilder.append(argument.charAt(i));
		}
		
		return stringBuilder.toString();
	}

	public static void main(String[] args) {
		String soliloquy = "Out, out, brief candle!";
		System.out.println(reverse(soliloquy));
	}

}
