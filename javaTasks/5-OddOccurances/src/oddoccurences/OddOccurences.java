package oddoccurences;

public class OddOccurences {
	//it would be better with histogram
	public static int getOddOccurrence(int... numbers){
		for (int i = 0; i < numbers.length; i++) {
			int currentNumber = numbers[i];
			int currentNumberOccurences = 0;
			
			for (int j = 0; j < numbers.length; j++) {
				if(numbers[j] == currentNumber){
					currentNumberOccurences++;
				}				
			}
			
			if(currentNumberOccurences % 2 != 0){
				return currentNumber;
			}
		}
		
		return 0;
	}

	public static void main(String[] args) {
		int[] numbers = {2,3,4,1,2,3,4,1,5,6,3,8};
		System.out.println(getOddOccurrence(numbers));
	}

}
