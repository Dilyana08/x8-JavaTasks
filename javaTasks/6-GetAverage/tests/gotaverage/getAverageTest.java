package gotaverage;

import static org.junit.Assert.*;

import org.junit.Test;

import gotaverage.getAverage;

public class getAverageTest {

	@Test(expected = AssertionError.class)
	public void testNullArray() {
		int[] numbers = null;
		getAverage.gotAverage(numbers);
	}
	
	@Test(expected = AssertionError.class)
	public void testEmptyArray() {
		int[] numbers = {};
		getAverage.gotAverage(numbers);
	}
	
	@Test
	public void testGetAverage(){
		int[] numbers = {1,3,4,6,5,2};
		int result = getAverage.gotAverage(numbers);
		assertEquals(3, result);
	}
	
	@Test
	public void testGetAverageSameElements(){
		int[] sameNmbersArray = {5,5,5,5,5};
		int result = getAverage.gotAverage(sameNmbersArray);
		assertEquals(5, result);
	}
	
	@Test
	public void testGetAverageOneElement(){
		int[] oneElement = {2};
		int result = getAverage.gotAverage(oneElement);
		assertEquals(2, result);
	}

}
